import { FC } from 'react';
import { Card, Col, Row } from 'react-bootstrap';
import { API_URL } from '../../config';
import { IVideo } from '../../hooks/useVideos/IVideo';

const VideoRow: FC<IVideoRowProps> = ({ video }) => (
  <Row className="my-3">
    <Col>
      <Card>
        <Card.Header as="h5" className="text-center">{video.filename}</Card.Header>
        <Card.Body>
          <video controls className="w-100" src={`${API_URL}${video.url}`} title={video.filename} />
        </Card.Body>
      </Card>
    </Col>
  </Row>
);

export default VideoRow;

interface IVideoRowProps {
  video: IVideo;
}