import { useMutation } from '@apollo/client';
import { loader } from 'graphql.macro';
import { useCallback } from 'react';
import { IVideo } from '../useVideos/IVideo';

const uploadMutation = loader('./uploadMutation.graphql');

export const useUploadVideo = () => {
  const [upload] = useMutation<{ upload: IVideo }, { file: File }>(uploadMutation, {});

  return useCallback((file: File) =>
    upload({
      variables: {
        file
      }
    }), [upload]);
};
