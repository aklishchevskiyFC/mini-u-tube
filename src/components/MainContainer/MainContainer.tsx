import React, { FC } from 'react';
import { Container } from 'react-bootstrap';
import { Route, Switch } from 'react-router-dom';
import ItemsList from '../ItemsList';
import UploadPage from '../UploadPage';

const MainContainer: FC = () => (
  <Container className="my-5" fluid="md">
    <Switch>
      <Route path="/upload">
        <UploadPage />
      </Route>
      <Route exact path="/">
        <ItemsList />
      </Route>
    </Switch>
  </Container>
  );

export default MainContainer;