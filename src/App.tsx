import { ApolloClient, ApolloProvider, InMemoryCache } from '@apollo/client';
import { createUploadLink } from 'apollo-upload-client';
import React, { FC, useMemo } from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import MainContainer from './components/MainContainer';
import { GRAPHQL_API_URL } from './config';

const App: FC = () => {
  const client = useMemo(() => new ApolloClient({
    link: createUploadLink({ uri: GRAPHQL_API_URL }),
    cache: new InMemoryCache(),
    defaultOptions: {
      query: {
        fetchPolicy: 'network-only'
      },
      watchQuery: {
        fetchPolicy: 'network-only'
      }
    }
  }), []);

  return (
    <ApolloProvider client={client}>
      <Router>
        <MainContainer />
      </Router>
    </ApolloProvider>
  );
};

export default App;
