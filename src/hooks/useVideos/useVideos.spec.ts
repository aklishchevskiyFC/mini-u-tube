import * as client from '@apollo/client';
import { QueryResult } from '@apollo/client/react/types/types';
import React from 'react';
import { useVideos } from '.';

beforeEach(() => {
  jest.spyOn(React, 'useMemo').mockImplementation(mem => mem());

  jest.spyOn(client, 'useQuery')
    .mockReturnValue({
      data: {
        videos: [
          video2,
          video3,
          video1
        ]
      }
    } as QueryResult<any, any>);
});

afterEach(() => {
  jest.restoreAllMocks();
});

it('should sort videos backwards', () => {
  const videos = useVideos();

  expect(videos).toEqual([
    video3,
    video2,
    video1
  ]);
});

const video1 = { id: '1', filename: '1', mimetype: '1', url: '1', encoding: '1', created: '1111' };
const video2 = { id: '2', filename: '2', mimetype: '2', url: '2', encoding: '2', created: '2222' };
const video3 = { id: '3', filename: '3', mimetype: '3', url: '3', encoding: '3', created: '3333' };