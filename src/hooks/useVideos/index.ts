import { useQuery } from '@apollo/client';
import { loader } from 'graphql.macro';
import { useMemo } from 'react';
import { IVideo } from './IVideo';

const videosQuery = loader('./videosQuery.graphql');

export const useVideos = () => {
  const { data: { videos = [] } = {} } = useQuery<{
    videos: Array<IVideo>;
  }, {
    first: number;
    count: number;
  }>(videosQuery, {});

  return useMemo(() => videos.slice().sort((a, b) => +b.created - +a.created), [videos]);
};