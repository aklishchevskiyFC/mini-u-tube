import React, { FC } from 'react';
import { Button, Col, Row } from 'react-bootstrap';
import { LinkContainer } from 'react-router-bootstrap';
import { useVideos } from '../../hooks/useVideos';
import VideoRow from './VideoRow';

const ItemsList: FC = () => {
  const videos = useVideos();

  return (
    <>
      <Row>
        <Col>
          <LinkContainer to="/upload">
            <Button block title="Go To Upload">Go To Upload</Button>
          </LinkContainer>
        </Col>
      </Row>
      {videos.map(video => <VideoRow key={video.id} video={video} />)}
    </>
  );
};

export default ItemsList;