import React from 'react';
import { render } from 'react-dom';
import { BrowserRouter as Router } from 'react-router-dom';
import * as useUploadVideo from '../../hooks/useUploadVideo';
import UploadPage from './UploadPage';

beforeEach(() => {
  jest.spyOn(useUploadVideo, 'useUploadVideo')
    .mockReturnValue(async () => ({}));
});

afterEach(() => {
  jest.restoreAllMocks();
});

it('should have back button', () => {
  const container = document.createElement('div');
  render(<Router><UploadPage /></Router>, container);

  expect(container.textContent).toContain('Go Back');
});

it('should have file selector', () => {
  const container = document.createElement('div');
  render(<Router><UploadPage /></Router>, container);

  expect(container.querySelector('input[type="file"]')).toBeDefined();
});

it('should have upload button', () => {
  const container = document.createElement('div');
  render(<Router><UploadPage /></Router>, container);

  expect(container.querySelector('button')).toBeDefined();
  expect(container.querySelector('button')?.textContent).toContain('Upload');
});
