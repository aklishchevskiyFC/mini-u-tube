import React, { FC, useCallback, useState } from 'react';
import { Button, Col, Form, Row } from 'react-bootstrap';
import { LinkContainer } from 'react-router-bootstrap';
import { useHistory } from 'react-router-dom';
import { useUploadVideo } from '../../hooks/useUploadVideo';

const UploadPage: FC = () => {
  const [file, setFile] = useState<File>();
  const { push } = useHistory();

  const handleFile = useCallback((e) => {
    setFile(e.target.files[0]);
  }, []);

  const uploadVideo = useUploadVideo();

  const handleUpload = useCallback(async () => {
    if (file) {
      await uploadVideo(file);
      push('/');
    }
  }, [push, uploadVideo, file]);

  return (
    <>
      <Row className="my-5">
        <Col>
          <LinkContainer to="/">
            <Button block title="Go Back">Go Back</Button>
          </LinkContainer>
        </Col>
      </Row>
      <Row className="my-5">
        <Col>
          <Form.File accept="video/*,*/*.mp4" onChange={handleFile} />
        </Col>
      </Row>
      <Row className="my-5">
        <Col>
          <Button block disabled={!file} onClick={handleUpload} title="Upload">Upload</Button>
        </Col>
      </Row>
    </>
  );
};

export default UploadPage;