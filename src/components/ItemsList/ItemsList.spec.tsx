import React from 'react';

import { render } from 'react-dom';
import { BrowserRouter as Router } from 'react-router-dom';
import *as useVideos from '../../hooks/useVideos';

import ItemsList from './ItemsList';

beforeEach(() => {
  jest.spyOn(useVideos, 'useVideos')
    .mockReturnValue([{
      id: 'test_id',
      created: '123',
      encoding: 'utf8',
      filename: 'test filename',
      mimetype: 'video/mp4',
      url: '/static/test_id'
    }]);
});

afterEach(() => {
  jest.restoreAllMocks();
});

it('should have navigation upload button', () => {
  const container = document.createElement('div');
  render(<Router><ItemsList /></Router>, container);

  expect(container.textContent).toContain('Go To Upload');
});

it('should have item in list', () => {
  const container = document.createElement('div');
  render(<Router><ItemsList /></Router>, container);

  expect(container.textContent).toContain('test filename');

  expect(container.querySelector('video')?.src).toBeDefined();
  expect(container.querySelector('video')?.src).toContain('/static/test_id');
});