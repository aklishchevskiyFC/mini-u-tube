import React from 'react';
import { render } from 'react-dom';
import VideoRow from './VideoRow';

it('should have video name', () => {
  const container = document.createElement('div');

  render(<VideoRow
    video={{
      id: 'test_id',
      created: '123',
      encoding: 'utf8',
      filename: 'test filename',
      mimetype: 'video/mp4',
      url: '/static/test_id'
    }}
  />, container);

  expect(container.textContent).toContain('test filename');
});

it('should have video url', () => {
  const container = document.createElement('div');

  render(<VideoRow
    video={{
      id: 'test_id',
      created: '123',
      encoding: 'utf8',
      filename: 'test filename',
      mimetype: 'video/mp4',
      url: '/static/test_id'
    }}
  />, container);

  expect(container.querySelector('video')?.src).toBeDefined();
  expect(container.querySelector('video')?.src).toContain('/static/test_id');
});
